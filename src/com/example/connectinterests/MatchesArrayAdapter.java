package com.example.connectinterests;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MatchesArrayAdapter extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] matches;
	
	static class ViewHolder {
		public TextView interests;
		public ImageView image;
	}
	
	public MatchesArrayAdapter(Activity context, String[] matches) {
		super(context, R.layout.fragment_matches, matches);
		this.context = context;
		this.matches = matches;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.fragment_matches, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.interests = (TextView) rowView.findViewById(R.id.skills_list);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.profile_pic);
			rowView.setTag(viewHolder);
			
		}
		ViewHolder holder = (ViewHolder) rowView.getTag();
		// parse the list of json strings
		String s = matches[position];
		try {
			JSONObject mainObject = new JSONObject(s);
			if (mainObject.has("username") && mainObject.has("skills")) {
				String skills = mainObject.getString("skills");
				String username = mainObject.getString("username");
				holder.interests.setText(username + " is interested in " + skills);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rowView;
	}


}
