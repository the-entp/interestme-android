package com.example.connectinterests;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CustomTextWatcher implements TextWatcher {
	private EditText txt;
	private Button save;
	
	
	public CustomTextWatcher(EditText e, Button s) {
		txt = e;
		save = s;
		
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		save.setVisibility(View.VISIBLE);
		
	}
	

}
