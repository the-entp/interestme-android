package com.example.connectinterests;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class MessagesArrayAdapter extends ArrayAdapter<String>{
	private final Activity context;
	private final String[] messengers;
	
	static class ViewHolder {
		public TextView messages;
		public ImageView image;
	}
	
	public MessagesArrayAdapter(Activity context, String[] messengers) {
		super(context, R.layout.fragment_messages, messengers);
		this.context = context;
		this.messengers = messengers;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.fragment_messages, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.messages = (TextView) rowView.findViewById(R.id.messages);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.profile_pic);
			rowView.setTag(viewHolder);
			
		}
		ViewHolder holder = (ViewHolder) rowView.getTag();
		// parse the list of json strings
		String s = messengers[position];
		holder.messages.setText("example");
		
		return rowView;
	}


}
