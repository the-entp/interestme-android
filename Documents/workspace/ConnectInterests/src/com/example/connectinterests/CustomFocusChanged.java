package com.example.connectinterests;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;

public class CustomFocusChanged implements OnFocusChangeListener {
	
	private EditText txt;
	private Button cancel;	
	public CustomFocusChanged(EditText e, Button c) {
		txt = e;
		cancel = c;
		
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (hasFocus) {
			cancel.setVisibility(View.VISIBLE);
		}
		
	}

}
