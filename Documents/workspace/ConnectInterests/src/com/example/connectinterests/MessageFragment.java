package com.example.connectinterests;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class MessageFragment extends ListFragment {
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.messages_list, container, false);
		String[] example = new String[1];
		example[0] = "example";
		MessagesArrayAdapter adapter = new MessagesArrayAdapter(getActivity(), example);
		setListAdapter(adapter);
		
		return rootView;
		
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent messaging = new Intent(getActivity(), MessagingActivity.class);
		startActivity(messaging);
	}
	

}
