package com.example.connectinterests;

import java.io.UnsupportedEncodingException;

import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileFragment extends Fragment {
	private static String user_obj;
	public static AsyncResponse main_ctx;
	private Intent pictureActionIntent = null;
	protected static final int CAMERA_REQUEST = 0;
	protected static final int GALLERY_PICTURE = 1;
	protected static final int RESULT_OK = -1;
	private Bitmap bitmap;
	private ImageView profile_pic;
	private String selectedImagePath;
	
	public static ProfileFragment newInstance(String user, AsyncResponse main) {
		ProfileFragment profile = new ProfileFragment();
		user_obj = user;
		main_ctx = main;
		return profile;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceStat) {
		View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
		LinearLayout profile_content = (LinearLayout) rootView.findViewById(R.id.profile_content);
		TextView profile_intro = (TextView) rootView.findViewById(R.id.profile_intro);
        TextView profile_skills = (TextView) rootView.findViewById(R.id.profile_skills);
        profile_pic = (ImageView) rootView.findViewById(R.id.profile_picture);
        profile_pic.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				startDialog();
				
			}
        	
        });
        try {
			JSONObject mainObject = new JSONObject(user_obj);
			if (mainObject.has("introduction")) {
				profile_intro.setText(mainObject.getString("introduction"));
			}
			if (mainObject.has("skills")) {
				profile_skills.setText(mainObject.getString("skills"));
			}
			if (mainObject.has("profile_pic")) {
				System.out.println("i am in proifle pic main object");
				String profile_pic_url = mainObject.getString("profile_pic");
				bitmap = BitmapFactory.decodeFile(profile_pic_url); // load
				bitmap = Bitmap.createScaledBitmap(bitmap,
						200, 100, false);
				System.out.println(bitmap == null);
				profile_pic.setImageBitmap(bitmap);
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		profile_content.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent edit_profile = new Intent(getActivity(), EditProfileActivity.class);
				edit_profile.putExtra("user_object", user_obj);
				startActivity(edit_profile);
			}
		});

        
		return rootView;
	}
	
	public void startDialog() {
		pictureActionIntent = new Intent(
                Intent.ACTION_GET_CONTENT, null);
        pictureActionIntent.setType("image/*");
        pictureActionIntent.putExtra("return-data", true);
        startActivityForResult(pictureActionIntent,
                GALLERY_PICTURE);
		
	}
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == GALLERY_PICTURE) {
			System.out.println("this is result code " + resultCode);
			if (resultCode == RESULT_OK) {
				if (data != null) {
					// our BitmapDrawable for the thumbnail
					BitmapDrawable bmpDrawable = null;
					// try to retrieve the image using the data from the intent
					System.out.println("This is data");
					System.out.println(data.getData());
					Cursor cursor = getActivity().getContentResolver().query(data.getData(),
							null, null, null, null);
					if (cursor != null) {

						cursor.moveToFirst();

						int idx = cursor.getColumnIndex(ImageColumns.DATA);
						String fileSrc = cursor.getString(idx);
						bitmap = BitmapFactory.decodeFile(fileSrc); // load
						// preview
						// image
						System.out.println("this is file source");
						System.out.println(fileSrc);
						System.out.println("this is image.getData");
						System.out.println(fileSrc);
						saveImage(fileSrc);
						bitmap = Bitmap.createScaledBitmap(bitmap,
								200, 100, false);
						profile_pic.setImageBitmap(bitmap);
					}
				} 
			}
		}
	}
	
	public void saveImage(String profile_pic_url) {
		StringEntity se;
		try {
			JSONObject mainObject = new JSONObject(user_obj);
			System.out.println("i am in save image");
			mainObject.put("profile_pic", profile_pic_url);
			String putURL = "http://" + Constants.IP_ADDR + "/users/edit";
			HttpPut put = new HttpPut(putURL);
			se = new StringEntity(mainObject.toString());
			se.setContentType("application/json");
			put.setEntity(se);
			RestTask r = new RestTask(getActivity(), "put");
			r.delegate = main_ctx;
			r.execute(put);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
