package com.example.connectinterests;

import java.io.UnsupportedEncodingException;

import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditProfileActivity extends Activity implements OnClickListener {
	private String user;
	private EditText edit_intro;
	private EditText edit_skills;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_profile);
		user = getIntent().getStringExtra("user_object");
		edit_intro = (EditText) findViewById(R.id.edit_introduction);
		edit_skills = (EditText) findViewById(R.id.edit_skills);
		try {
			JSONObject mainObject = new JSONObject(user);
			if (mainObject.has("introduction")) {
				
				edit_intro.setText(mainObject.getString("introduction"));
			}
			if (mainObject.has("skills")) {
				edit_skills = (EditText) findViewById(R.id.edit_skills);
				edit_skills.setText(mainObject.getString("skills"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        final Button cancel = (Button) findViewById(R.id.cancel);
        final Button save = (Button) findViewById(R.id.save);
        cancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
       save.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_profile, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		try {
			
			JSONObject mainObject = new JSONObject(user);
			System.out.println("this is user");
			System.out.println(user);
			String username = mainObject.getString("username");
			mainObject.put("introduction", edit_intro.getText().toString());
			mainObject.put("skills", edit_skills.getText().toString());
			String putURL = "http://" + Constants.IP_ADDR + "/users/edit";
			HttpPut put = new HttpPut(putURL);
			StringEntity se = new StringEntity(mainObject.toString());
			se.setContentType("application/json");
			put.setEntity(se);
			RestTask r = new RestTask(v.getContext(), "put");

			r.delegate = ProfileFragment.main_ctx;
			r.execute(put);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finish();

	}



}
